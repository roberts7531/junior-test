-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 24, 2019 at 06:20 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mybase`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `ID` int(10) NOT NULL,
  `SKU` varchar(10) NOT NULL,
  `NAME` varchar(20) NOT NULL,
  `PRICE` float NOT NULL,
  `TYPE` int(1) NOT NULL,
  `ATTR` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`ID`, `SKU`, `NAME`, `PRICE`, `TYPE`, `ATTR`) VALUES
(44, 'jogo', 'roberts', 123124, 3, '420x420x420'),
(48, 'jogo', 'roberts', 123124, 3, '420x420x420'),
(49, 'jogo', 'roberts', 123124, 3, '420x420x420'),
(52, 'jogo', 'roberts', 123124, 3, '420x420x420'),
(53, 'jogo', 'roberts', 123124, 3, '420x420x420'),
(54, 'jogo', 'roberts', 123124, 3, '420x420x420'),
(56, 'jogo', 'roberts', 123124, 3, '420x420x420'),
(57, 'jogo', 'roberts', 123124, 3, '420x420x420'),
(58, 'lo;k;', 'roberts', 123, 1, '66'),
(59, 'lo;k;', 'gu', 1234, 1, '66'),
(60, 'lo;k;', 'gu', 1234, 1, '66'),
(61, 'lo;k;', 'gu', 1234, 1, '66'),
(62, 'joksis', 'roberts', 1234, 1, '66');

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `ID` int(11) NOT NULL,
  `TYPE` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`ID`, `TYPE`) VALUES
(1, 'DVD'),
(2, 'Book'),
(3, 'Furniture');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `TYPE` (`TYPE`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `TYPE` FOREIGN KEY (`TYPE`) REFERENCES `types` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
